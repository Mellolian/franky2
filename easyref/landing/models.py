from django.db import models


# Create your models here.
class ArticleModel(models.Model):
    choices = (('1', 'ГОСТ'), ('2', 'RSC'), ('3', 'ACS'))
    doi = models.CharField(max_length=100)
    title = models.TextField()
    authors = models.TextField()
    journal = models.TextField()
    year = models.IntegerField()
    volume = models.IntegerField()
    issue = models.IntegerField()
    pages = models.TextField()
    formate = models.CharField(max_length=10, choices=choices)



class FeedbackModel(models.Model):
    name = models.CharField(max_length=30)
    suggestion = models.TextField()

