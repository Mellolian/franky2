import psycopg2 as psycopg2
from django.shortcuts import render
from django.http import request, response, HttpResponse
from .forms import ArticleForm, FeedbackForm
import psycopg2
from docx import Document
from docx.shared import Inches, Pt

# Create your views here.
def favicon(request):
    image_data = open("/static/favicon.ico", "rb").read()
    return HttpResponse(image_data, mimetype="image/png")

def author_list(authorString):
    if ' and ' in str(authorString):
        authorsstr = str(authorString).split(' and ')
        authorslist = authorsstr[0].split(',')
        authorslist.append(authorsstr[1])

    elif ',' in authorString:
        authorslist = authorString.split(',')
    else:
        authorslist = str(authorString).strip(' ')
    return authorslist

def SurnamePlusInitials(authorlist):
    authorstotal = ''
    if isinstance(authorlist, list):
        for i in authorlist:
            i = i.strip('{}"').strip("'")  # убираем лишние символы
            surname = i.split(' ')[-1]  # фамилия
            initials = surname + ','
            # print(initials)
            for name in i.split(' ')[:-1]:
                if name:
                    initials += ' ' + name[0] + '.'
            authorstotal += initials + '; '
        print(authorstotal.strip('; '))

    elif isinstance(authorlist, str): #один автор, передается строка
        surname = authorlist.split(' ')[-1]
        initials = surname + ','
        for name in authorlist.split(' ')[:-1]:
            if name:
                initials += ' ' + name[0] + '.'
        authorstotal += initials + '; '

    return str(authorstotal.strip('; ')) + ' '


def InitialsPlusSurname(authorlist):
    if isinstance(authorlist, list):
        authorstotal = ' '
        for author in authorlist:
            for name in author.split(' ')[:-1]:
                initials = ''
                if name:
                    name = name.strip('. ')
                    initials += name[0] + '. '
                    authorstotal += initials
            surname = author.split(' ')[-1]
            authorstotal += surname+', '
        return authorstotal.strip(', ')

    elif isinstance(authorlist, str): #один автор, передается строка
        surname = authorlist.split(' ')[-1]
        authorstotal = ' '
        for name in authorlist.split(' ')[:-1]:
            initials = ''
            if name:
                name = name.strip('. ')
                authorstotal += ' ' + name[0] + '.'
        authorstotal += ' ' + surname
    else:
        print('ERROR')
        authorstotal = ' '
    return authorstotal + ' '


def index(request):

    form = ArticleForm(request.POST or None)
    if request.method == "POST" and "btnform1" in request.POST:

        print(request.POST['doi'])
        formate = request.POST['formate']

        filename = str(request.POST['doi'][8:]) + str(request.POST['formate']) + '.docx'
        hostname = 'database1'
        username = 'postgres'
        password = 'postgres'  # your password
        database = 'ACS'
        port = '5432'
        connection = psycopg2.connect(host=hostname,
                                      user=username,
                                      password=password,
                                      dbname=database,
                                      port=port
                                      )
        cur = connection.cursor()
        print('connected')
        cur.execute('SELECT * FROM articles WHERE doi = %s;', (request.POST['doi'],))
        cr = cur.fetchone()
        print(cr)
        if cr is not None:
            article_name = str(cr[1])
            authors = author_list(str(cr[2]))
            journal_name = str(cr[3])
            year = str(cr[4])
            volume = str(cr[5])
            issue = str(cr[6])
            pages = str(cr[7])
            response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
            if request.POST['formate'] == '1': #GOST
                print(request.POST['formate'])
                document = Document()
                font = document.styles['Normal'].font
                font.name = 'Times New Roman'
                font.size = Pt(14)

                document.add_paragraph('https://dx.doi.org/' + str(request.POST['doi']))
                if len(authors) >=5 and isinstance(authors, list):
                    print('authors is list and length '+str(len(authors)))
                    p = document.add_paragraph(article_name.strip(' ') + ' / ')
                    p.add_run(', '.join((InitialsPlusSurname(authors[0:5]).split(';'))).strip(' ') + ' [и др.] // ')
                    p.add_run(journal_name.strip('{}"').strip("'") + ' - ')
                    p.add_run(year + '. - ')
                    p.add_run('Т. '+volume + ', № '+issue+ '. - ')
                    p.add_run('C. ' + pages+ '.')

                elif len(authors) ==4 and isinstance(authors, list):
                    print('authors is list and length'+str(len(authors)))
                    p = document.add_paragraph(article_name.strip(' ') + ' / ')
                    p.add_run(', '.join((InitialsPlusSurname(authors).split(';'))).strip(' ') + '// ')
                    p.add_run(journal_name.strip('{}"').strip("'") + ' - ')
                    p.add_run(year + '. - ')
                    p.add_run('Т. '+volume + ', № '+issue+ '. - ')
                    p.add_run('C. ' + pages+ '.')

                elif len(authors) >=2 and isinstance(authors, list):
                    p = document.add_paragraph(SurnamePlusInitials(str(authors[0])))
                    p.add_run(article_name.strip(' ') + ' / ')
                    p.add_run(', '.join((InitialsPlusSurname(authors).split(';'))).strip(' ') + '// ')
                    p.add_run(journal_name.strip('{}"').strip("'") + ' - ')
                    p.add_run(year + '. - ')
                    p.add_run('Т. '+volume + ', № '+issue+ '. - ')
                    p.add_run('C. ' + pages + '.')

                elif isinstance(authors, str):
                    p = document.add_paragraph(str(SurnamePlusInitials(authors)))
                    p.add_run(article_name.strip(' ') + ' / ')
                    p.add_run(', '.join((InitialsPlusSurname(authors).split(';'))).strip(' ') + '// ')
                    p.add_run(journal_name.strip('{}"').strip("'") + ' - ')
                    p.add_run(year + '. - ')
                    p.add_run('Т. '+volume + ', № '+issue+ '. - ')
                    p.add_run('C.' + pages+ '.')

                filename = str(request.POST['doi'][8:]) + str(request.POST['formate']) + '.docx'
                document.save(response)

            elif request.POST['formate'] == '2': #RSC
                print(request.POST['formate'])
                document = Document()
                font = document.styles['Normal'].font
                font.name = 'Times New Roman'
                font.size = Pt(14)
                document.add_paragraph('https://dx.doi.org/' +str(request.POST['doi']))
                p = document.add_paragraph(InitialsPlusSurname(authors) + ', ')
                p.add_run(str(journal_name.strip('{}"').strip("'") + ', ')).italic = True
                p.add_run(year + ', ')
                p.add_run(volume + ', ').bold = True
                p.add_run(issue + ', ')
                p.add_run(pages + '.')
                filename = str(request.POST['doi'][8:]) + str(request.POST['formate']) + '.docx'
                document.save(response)

            elif request.POST['formate'] == '3': #ASC
                document = Document()
                font = document.styles['Normal'].font
                font.name = 'Times New Roman'
                font.size = Pt(14)

                if ' and ' in authors:
                    authorsstr = authors.split(' and ')
                    authorslist = authorsstr[0].split(',')
                    authorslist.append(authorsstr[1])
                    authorstotal = SurnamePlusInitials(authorslist)
                else:
                    authorslist = authors
                    print(authors)
                    authorstotal = SurnamePlusInitials(authorslist)

                document.add_paragraph('https://dx.doi.org/' + str(request.POST['doi']))
                p = document.add_paragraph(authorstotal)
                p.add_run(str(journal_name.strip('{}"').strip("'")) + ' ').italic = True
                p.add_run(year + ', ').bold = True
                p.add_run(volume + ', ').italic = True
                p.add_run(issue + ', ')
                p.add_run(pages + '.')
                filename = str(request.POST['doi'][8:]) + str(request.POST['formate']) + '.docx'
                document.save(response)
            response['Content-Disposition'] = 'attachment; filename='+filename
            return response
        else:
            with open('NotFoundDoi.txt', 'a') as f:
                f.write(str(request.POST['doi'])+'\n'+ '\n')

            return HttpResponse('<h1>Article is not in the database yet or wrong DOI format :(</h1>')

    form2 = FeedbackForm(request.POST or None)
    if request.method == "POST" and "btnform2" in request.POST:
        with open('suggestions.txt', 'a') as f:
            f.write(str(request.POST['name']) + ':' + str(request.POST['suggestion']) + '\n'+ '\n')
        print(request.POST['name'])
        print(request.POST['suggestion'])
        return HttpResponse("<h1> Thank you for your suggestion, we'll try to update our base ASAP</h1>")

    return render(request, 'index.html', locals())


def DOI(request):
    with open('NotFoundDoi.txt', 'r') as f:
        return HttpResponse(f.read())

def sug(request):
    with open('suggestions.txt', 'r') as f:
        return HttpResponse(f.read())
