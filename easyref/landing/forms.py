from django import forms
from .models import *
from crispy_forms.helper import FormHelper

class ArticleForm(forms.ModelForm):

    helper = FormHelper()
    helper.form_show_labels = False

    class Meta:
        model = ArticleModel
        fields = ["doi", "formate"]
        choices = (('1', 'ГОСТ'), ('2', 'RSC'), ('3', 'ACS'))
        widgets = {
            'doi': forms.Textarea(attrs={'rows': 1, 'placeholder': '10.1039/c7gc03626d'}),
            'formate': forms.Select(attrs={'rows': 1, 'placeholder': 'Choose format'}),
        }
        labels = {
            "private": "Keep Private",
        }


class FeedbackForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_show_labels = False
#
    class Meta:
        model = FeedbackModel
        fields = ["name", "suggestion"]
        widgets = {
            'name': forms.Textarea(attrs={'rows': 1, 'placeholder': 'Your name or email'}),
            'suggestion': forms.Textarea(attrs={'rows': 4, 'placeholder': 'Your suggestion or question'}),
        }
        labels = {
            "private": "Keep Private",
        }